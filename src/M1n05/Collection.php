<?php

namespace M1n05;

class Collection implements \Countable, \ArrayAccess, \IteratorAggregate, \Serializable  {


	private $elements =  [];


	public function __construct($elements = array()){
		if(is_array($elements)){
			foreach ($elements as $key => $value) {
				if(is_array($value)){
					$element = new Collection($value);
				} else {
					$element = $value;
				}
				$this->elements[$key] = $element;
			}
		}
	}


	public function exists($name){
		return isset($this->elements[$name]);
	}

	public function get($name, $defaultValue = null){
		$keys = explode('>', $name);

		$firstKey = array_shift($keys);

		if($this->exists($firstKey)){
			$element = $this->elements[$firstKey];
			
			if(count($keys) === 0){
				return $element;
			} else {
				if($element instanceof Collection){
					return $element->get(implode('>', $keys), $defaultValue);
				}
			}
		}
		return $defaultValue;
	}

	public function set($name, $value){
		$this->elements[$name] = $value;
		return $this;
	}

	public function map($callback){
		return new Collection(array_map($callback, $this->elements));
	}

	public function filter($callback){
		return new Collection(array_filter($this->elements, $callback));
	}

	// Implémentation de Countable

	public function count(){
		return count($this->elements);
	}


	//Implémentation de ArrayAccess
	
	public function offsetExists($name){
		return $this->exists($name);
	}

	public function offsetGet($name){
		return $this->get($name, '');
	}

	public function offsetSet($name, $value){
		return $this->set($name, $value);
	}

	public function offsetUnset($name){
		unset($this->elements[$name]);
	}

	//Implementation de IteratorAggregate
	
	public function getIterator() {
        return new \ArrayIterator($this->elements);
    }

    //Implementation de Serializable
    //
    public function serialize(){
    	return serialize($this->elements);
    }

    public function unserialize($data){
    	$this->elements = unserialize($data);
    }

    public function toArray(){
    	$elements = array_map(function($element){
            if($element instanceof Collection){
                return $element->toArray();
            } else {
                return $element;
            }
        }, $this->elements);
        
        return $elements;
    }

}